import socket
import os
import os.path
import time
from threading import Lock
from random import randint

#A Node class representing a client or a Server
class MyNode(object):
	from threading import Thread

	#portList beinhaltet die ports aller sockets zu denen der node verbunden ist
	def __init__(self):
		print("Node "+str(id(self)) + " created")
		self.sockSendingList = []
		self.sockReceivingList = []
		self.msgQueue = []
		self.lock = Lock()
		self.inputActions = []
		self.myNo = 0
		self.allNodes = 0
		self.ticks = -1
		self.msgQueue.append(("INIT",-1))

	def addSendingPort(self,portNo,nodeNo):
		print("Assigning sending Port "+str(portNo)+" To "+str(id(self)))
		sock = socket.socket(socket.AF_UNIX,socket.SOCK_DGRAM)
		sock.connect("/tmp/mysock"+str(portNo))
		self.sockSendingList.append((sock,nodeNo))

	def addListeningPort(self,portNo,nodeNo):
		print("Assigning listening Port "+str(portNo)+" To "+str(id(self)))
		if(os.path.exists("/tmp/mysock"+str(portNo))):
			os.remove("/tmp/mysock"+str(portNo))
		sock = socket.socket(socket.AF_UNIX,socket.SOCK_DGRAM)
		sock.bind("/tmp/mysock"+str(portNo))
		self.sockReceivingList.append((sock,nodeNo))

	#Things the Thread does
	def threadFunction(self, a):
		print("Thread %d in " %a +str(id(self)))
		#while(True):
		#print("Sending message to " + str(a))
		#self.sockSendingList[a].send('Hi')
		while True:
			data = self.sockReceivingList[a][0].recv(1024)
			if not data:
				print("No data received")
			else:
				if(data == "tic"):
					#print("Received tic, performing algorithm and returning ack")
					self.ticks+=1
					#run stuff
					self.lock.acquire()
					self.checkInputActions()
					self.lock.release()
					#self.sockSendingList[a][0].send('ack')
					self.sendTo("ack",-1)
				else:
					#print("Received " +str(data))
					self.lock.acquire()
					self.msgQueue.append((str(data),self.sockReceivingList[a][1]))
					self.lock.release()

	#starts a Thread for every ListeningSocket
	def runAlgo(self):
		print("---Run Algo for Node "+str(id(self)))
		for k in range(0,len(self.sockReceivingList)):
			t = self.Thread(target=self.threadFunction, args=(k,))
			t.start()
			#time.sleep(1)
			#print(self.msgQueue)

	def checkInputActions(self):
		possibleActions = []

		for inputAction in self.inputActions:
			for message in self.msgQueue:
				if(inputAction[0](message) ==  True):
					possibleActions.append((inputAction[1],message))
		if(len(possibleActions)==0):
			return
		rand = randint(0,len(possibleActions)-1)
		possibleActions[rand][0](possibleActions[rand][1])

	def sendTo(self,msg,nodeNo):
		for socket in self.sockSendingList:
			if(socket[1] == nodeNo):
				socket[0].send(msg)

#The tacting node for the synchronous algortihm
class Metronom(MyNode):

	def __init__(self):
		MyNode.__init__(self)
		self.acks = 0
		print("Metronom")

	#Things the Thread does
	def threadFunction(self, a):
		while True:
			self.sockSendingList[a][0].send('tic')
			while (True):
				data = self.sockReceivingList[a][0].recv(1024)
				if data == "ack":
					self.lock.acquire()
					self.acks+=1
					if(self.acks == len(self.sockReceivingList)):
							self.acks = 0
							time.sleep(2)
							for socket in self.sockSendingList:
								socket[0].send('tic')
					self.lock.release()
				else:
					print("Unknown data received "+str(data))

	#starts a Thread for every ListeningSocket
	def runAlgo(self):
		print("---Run Algo for Metronom "+str(id(self)))
		for k in range(0,len(self.sockReceivingList)):
			t = self.Thread(target=self.threadFunction, args=(k,))
			t.start()

#To test your algorithm
class Playground(object):

	def __init__(self,initNodeList,topology):
		self.nodeList = []
		self.metronom = Metronom()
		self.portCounter = 10000
		self.myTopology = topology
		self.nodeList = initNodeList

		#adding Sending and Receiving Sockets
		for idx,node in enumerate(self.nodeList):
			node.myNo = idx
			node.allNodes = len(self.nodeList)
			for j in range(0,len(topology[idx])):
				if(topology[idx][j]==1):
					self.nodeList[j].addListeningPort(self.portCounter,idx)
					#node.addListeningPort(self.portCounter,j)
					self.nodeList[idx].addSendingPort(self.portCounter,j)
					self.portCounter+=1
				#conecting all nodes with the metronom
			node.addListeningPort(self.portCounter,-1)
			self.metronom.addSendingPort(self.portCounter,idx)
			self.portCounter+=1
			self.metronom.addListeningPort(self.portCounter,idx)
			node.addSendingPort(self.portCounter,-1)
			self.portCounter+=1

		#Starts Algorithm for every node in nodeList
		for k in range(0,len(self.nodeList)):
			#print(self.nodeList[k].sockSendingList)
			#print(self.nodeList[k].sockReceivingList)
			self.nodeList[k].runAlgo()
		self.metronom.runAlgo()



#-----------------------------------------------------------

class TokenRingNode(MyNode):

	def __init__(self):
		MyNode.__init__(self)
		self.inputActions.append((self.input0,self.action0))
		self.inputActions.append((self.input1,self.action1))
		self.inputActions.append((self.input2,self.action2))
		self.CR_needed = False

	def input0(self,message):
		if(self.ticks == 0):
			return True
		else:
			return False

	def action0(self,message):
		if(self.myNo == 1):
			print("Sending token initially")
			self.sendTo("token",self.myNo+1)
			self.msgQueue.remove(message)

	def input1(self,message):
		#print("Checking Input Condition 1")
		if(self.ticks > 0 and message[0] == "load" and message[1] == 0):
			return True
		return False

	def action1(self,message):
		print("Node number "+str(self.myNo)+" was told to reserve the ressource")
		self.CR_needed = True
		self.msgQueue.remove(message)

	def input2(self,message):
		if(self.ticks > 0 and message[0] == "token" and message[1] != 0):
			return True
		return False

	def action2(self,message):
		print(str(message[1])+" --> "+str(self.myNo) )
		if(self.CR_needed == True):
			print("Node No "+str(self.myNo)+" is doing sth")
			self.CR_needed = False
		sendToNo = self.myNo+1
		if(sendToNo > (self.allNodes-1)):
			sendToNo = 1
		self.sendTo("token",sendToNo)
		self.msgQueue.remove(message)


class TokenRingServerNode(MyNode):
	def __init__(self):
		MyNode.__init__(self)
		self.inputActions.append((self.input0,self.action0))

	def input0(self,message):
		return True

	def action0(self,message):
		rnd0 = randint(1,self.allNodes-1)
		rnd1 = randint(1,100)
		if(rnd1 <= 33):
			self.sendTo('load',rnd0)
			print("Told The Node "+str(rnd0)+" To reserve the ressource if possible")


#------------------------

nodeList = []
nodeList.append(TokenRingServerNode())
for _ in range(0,3):
	nodeList.append(TokenRingNode())

pg = Playground(nodeList,[[0,1,1,1],[1,0,1,0],[1,0,0,1],[1,1,0,0]])
